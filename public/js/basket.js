/**
 * This method handles the click of the add button
 */
function addButtonClicked(currProdId){
    //we have to first retrieve the amount we need to add to our basket
    let spinner = document.getElementById("ni-" + currProdId);
    let amount = spinner.value;

    //We do not want to add less than one item
    if(amount < 1 ){
        alert("Can't add less then 1 item to basket!");
        return;
    }


    //Getting the Basket from SessionStorage
    let basketJson = window.sessionStorage.getItem("basket");
    let basket;
    if(basketJson == null){
        basket = [];
    }
    else{
        basket = JSON.parse(basketJson);
    }

    //Setting the new value
    let index = has(basket, currProdId);
    if(index === -1){
        //Add new object with key and amount to array
        basket.push({key: currProdId, amount: parseInt(amount)});
    }
    else{
        //add the old and the new amount
        let entry = basket[index];
        entry.amount = parseInt(entry.amount) + parseInt(amount);
        basket[index] = entry;
    }

    //Write Basket back to SessionStorage
    window.sessionStorage.setItem("basket", JSON.stringify(basket));

    spinner.value = 0;

    //Displaying success message
    let succMsgDiv = document.getElementById("msgsucc-" + currProdId);
    succMsgDiv.style.display = "block";
    setTimeout(() => {
        succMsgDiv.style.display = "none"
    }, 3000);
}


function has(array, key){
    for(let i = 0; i < array.length; i++){
        if(array[i].key === key){
            return i;
        }
    }
    return -1;
}