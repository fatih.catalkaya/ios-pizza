window.onload = (ev) => {
    let xmlHttp = new XMLHttpRequest();
    xmlHttp.onreadystatechange = function() {
        if (xmlHttp.readyState === 4 && xmlHttp.status === 200){
            let products = xmlHttp.responseText;
            window.sessionStorage.setItem("products", products);

            buildBasket();
        }
    }
    xmlHttp.open("GET", "/products", true); // true for asynchronous
    xmlHttp.send(null);
}


function buildBasket() {

    //Build Basket
    let products = JSON.parse(window.sessionStorage.getItem("products"));
    let basket = JSON.parse(window.sessionStorage.getItem("basket"));
    let numberFormat = new Intl.NumberFormat('de-DE', { style: 'currency', currency: 'EUR' });

    //Set Checkout Button Disabled if there are no items
    if(basket == null || basket.length === 0){
        document.getElementById("checkoutBtn").setAttribute("disabled", 1);
        return;
    }


    for(let i = 0; i < basket.length; i++){

        let product = null;
        let amount = basket[i].amount;

        for(let j = 0; j < products.length; j++){
            if(products[j].id === basket[i].key){
                product = products[j];
            }
        }

        //Product was not found, dont display it
        if(product === null){
            continue;
        }


        let templateClone = document.getElementById("template").content.cloneNode(true);
        templateClone.querySelectorAll(".product")[0].setAttribute("data-prodId", product.id);

        templateClone.querySelectorAll(".name")[0].innerText = product.name;
        templateClone.querySelectorAll(".desc")[0].innerText = product.desc;
        templateClone.querySelectorAll(".price")[0].innerText = numberFormat.format(product.price);
        templateClone.querySelectorAll("img")[0].src = "/img/" + product.img;

        let templateSpinner = templateClone.querySelectorAll("input[type=number]")[0];
        templateSpinner.setAttribute("data-prodId", product.id);
        templateSpinner.setAttribute("value", amount);

        templateClone.querySelectorAll("input[type=button]")[0].setAttribute("data-prodId", product.id);

        document.getElementById("products").appendChild(templateClone);
    }
}


function onSpinnerChange(evtSrc){
    let productId = evtSrc.getAttribute("data-prodId");
    let basket = JSON.parse(window.sessionStorage.getItem("basket"));

    for(let i = 0; i < basket.length; i++){
        if(basket[i].key === productId){
            basket[i].amount = parseInt(evtSrc.value);
            break;
        }
    }

    window.sessionStorage.setItem("basket", JSON.stringify(basket));
}


function onRemoveClicked(evtSrc){
    let productId = evtSrc.getAttribute("data-prodId");

    //Remove from DOM
    document.querySelectorAll("div[data-prodId =" + productId +" ]")[0].remove();

    //Remove from Basket
    let basket = JSON.parse(window.sessionStorage.getItem("basket"));
    for(let i = 0; i < basket.length; i++){
        if(basket[i].key === productId){
            basket.splice(i, 1);
            break;
        }
    }

    //Set Checkout Button Disabled if there are no items left
    if(basket.length === 0){
        document.getElementById("checkoutBtn").setAttribute("disabled", 1);
    }

    window.sessionStorage.setItem("basket", JSON.stringify(basket));
}