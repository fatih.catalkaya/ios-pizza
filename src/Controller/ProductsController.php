<?php


namespace App\Controller;


use App\Entities\Pasta;
use App\Entities\Pizza;
use App\Entities\Product;
use App\Entities\Salad;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class ProductsController extends AbstractController
{
    /**
     * @Route("/specials", name="specials")
     */
    public function showSpecials() : Response {
        $products = $this->getSpecials();
        return $this->render("products.html.twig", array("products" => $products));
    }


    /**
     * @Route("/pizzas", name="pizzas")
     */
    public function showPizzas() : Response {
        $products = $this->getPizzas();
        return $this->render("products.html.twig", array("products" => $products));
    }


    /**
     * @Route("/pastas", name="pastas")
     */
    public function showPasta() : Response {
        $products = $this->getPastas();
        return $this->render("products.html.twig", array("products" => $products));
    }


    /**
     * @Route("/salads", name="salads")
     */
    public function showSalads() : Response {
        $products = $this->getSalads();
        return $this->render("products.html.twig", array("products" => $products));
    }


    /**
     * @Route("/products", name="api_products")
     */
    public function getProducts() : Response {
        $productArray = array_merge($this->getSpecials(), $this->getPizzas(), $this->getPastas(), $this->getSalads());
        return new JsonResponse($productArray);
    }


    private function getSpecials(){
        $s1 = new Product("s1", "Special Offer #1", "2 Pizza + 1L drink", 12.30, "special.jpg");
        $s2 = new Product("s2", "Special Offer #2", "2 Pasta + 1L drink", 10.30, "special.jpg");
        $s3 = new Product("s3", "Special Offer #3", "1 Pizza + 0.5L drink", 5.30, "special.jpg");
        $s4 = new Product("s4", "Special Offer #4", "1 Pasta + 0.5L drink", 6.20, "special.jpg");

        return array($s1, $s2, $s3, $s4);
    }


    private function getPizzas(){
        $s1 = new Pizza("pi1", "Pizza Margherita", "", 4.5);
        $s2 = new Pizza("pi2", "Pizza Salami", "With exclusive Italian salami", 5.5);
        $s3 = new Pizza("pi3", "Pizza Tonno", "With tuna and onions", 6.0);
        $s4 = new Pizza("pi4", "Pizza Vegetariano", "With mushrooms, onions, paprika, pepperoni, broccoli", 7.5);
        $s5 = new Pizza("pi5", "Pizza Quattro Formaggio", "With four cheeses", 7.0);
        $s6 = new Pizza("pi6", "Pizza Hawaii", "With ham and pineapple", 6.5);

        return array($s1, $s2, $s3, $s4, $s5, $s6);
    }


    private function getPastas(){
        $s1 = new Pasta("pa1", "Pasta Combinazione", "With rigatoni, spaghetti, bolognese and gratinated with cheese", 6.6);
        $s2 = new Pasta("pa2", "Pasta Bolognese", "", 6.6);
        $s3 = new Pasta("pa3", "Pasta Carbonara", "With ham, cream, egg and parmesan", 6.9);
        $s4 = new Pasta("pa4", "Pasta Napoli", "With tomato sauce", 6.6);
        $s5 = new Pasta("pa5", "Pasta Aglio e Olio", "With olive oil and garlic", 6.60);

        return array($s1, $s2, $s3, $s4, $s5);
    }


    private function getSalads(){
        $s1 = new Salad("sa1", "Salad Mista", "With paprika, onions and corn", 4.0);
        $s2 = new Salad("sa2", "Caesar Salad", "With chicken breast and parmesan", 7.4);
        $s3 = new Salad("sa3", "Salad Pollo", "With chicken breast, mushrooms, onions and peppers", 7.4);
        $s4 = new Salad("sa4", "Salad Tonno", "With tuna and onions", 7.0);
        $s5 = new Salad("sa5", "Salad Capricciosa", "With ham, tuna and egg", 7.0);

        return array($s1, $s2, $s3, $s4, $s5);
    }
}