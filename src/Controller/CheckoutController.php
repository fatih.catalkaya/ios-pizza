<?php


namespace App\Controller;


use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CheckoutController extends AbstractController
{
    /**
     * @Route("/checkout", name="checkout")
     */
    public function checkoutForm() : Response {
        return $this->render("checkout.html.twig");
    }


    /**
     * @Route("/success", name="success")
     */
    public function showSuccess() : Response {
        return $this->render("success.html.twig");
    }
}