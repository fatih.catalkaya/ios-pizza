<?php


namespace App\Entities;


class Pasta extends Product
{
    public function __construct($id, $name, $desc, $price)
    {
        parent::__construct($id, $name, $desc, $price, "pasta.jpg");
    }
}