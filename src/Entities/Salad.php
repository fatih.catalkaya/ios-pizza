<?php


namespace App\Entities;


class Salad extends Product
{
    public function __construct($id, $name, $desc, $price)
    {
        parent::__construct($id, $name, $desc, $price, "salad.jpg");
    }
}