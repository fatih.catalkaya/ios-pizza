<?php


namespace App\Entities;


class Pizza extends Product
{
    public function __construct($id, $name, $desc, $price)
    {
        parent::__construct($id, $name, $desc, $price, "pizza.jpg");
    }
}